# Getting Started 

### Prerequisites

- Run the news-api locally, clone https://gitlab.com/tayoo/news-api.git and follow the instructions
- in this project, make sure you're pointing to the right URL in services/newsApi.js

### Run the app

- `npm install`
- `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
