import * as React from "react";
import { styled } from "@mui/material/styles";
import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Collapse,
  Typography
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Link from "@mui/material/Link";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest
  })
}));

export default function Article ({data}) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card style={{marginLeft: 20, marginRight: 20, marginBottom: 20, maxWidth: 350}}>
      <CardHeader
        title={<Link href={data.url}>{data.title}</Link>}
        subheader={
          <Typography
            variant="caption"
            display="block"
            gutterBottom
            color="gray"
          >
            {new Date(data.publishedAt).toDateString()} <br /> By{" "}
            {data.source.name}
          </Typography>
        }
      />
      <CardMedia
        component="img"
        height="194"
        image={data.urlToImage}
        alt={data.title}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {data.description}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>{data.content}</Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
};
