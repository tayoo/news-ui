import * as React from "react";
import {Grid, Box} from '@mui/material';
import Article from './Article'

export default function Articles ({data}) {

  return (
    <Box>
        <Grid container justifyContent='center'>
            {data.map((row, index) => {
                return <Article key={index} data={row}/>
            })}
        </Grid>
    </Box>
  );
};