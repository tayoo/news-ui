import * as React from 'react';
import {Box, TextField, AccordionDetails, AccordionSummary, Accordion, Grid, InputAdornment,
    FormControl, InputLabel, Select, MenuItem, Typography, LinearProgress, Button, Avatar} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import SearchIcon from '@mui/icons-material/Search';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import dayjs from 'dayjs';
import logo from '../assets/news-logo.jpg'

export default function Filter(props) {
    const {reloadParent, loading} = props
    const [query, setQuery] = React.useState("Dune Tech");
    const [fromDate, setFromDate] = React.useState(dayjs((new Date()).toDateString()));
    const [toDate, setToDate] = React.useState("");
    const [sortBy, setSortBy] = React.useState("popularity");
    const handleSortChange = (event) => {
        setSortBy(event.target.value);
    };

    const onSubmit = async () => {
        await reloadParent({
            query,
            from: (new Date(fromDate)).toISOString().split('T')[0],
            to: toDate? (new Date(toDate)).toISOString().split('T')[0]: toDate,
            sortBy
        })
    }

    const handleKeyDown = async (event) => {
        if (event.key === 'Enter') {
            await onSubmit()
        }
      }
    
  return (
      <Box>
        <Avatar alt="Russel Crow" src={logo} />
         <Typography variant="h4" gutterBottom>
            News Dashboard
        </Typography>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            >
            <TextField
                fullWidth  id="standard-basic" label="Focus your search" variant="standard" value={query} onChange={(event) => { setQuery(event.target.value) }}
                InputProps={{
                    startAdornment: (
                    <InputAdornment position="start">
                        <SearchIcon />
                    </InputAdornment>
                    ),
                }}
                onKeyDown={handleKeyDown}
                />
            
            </AccordionSummary>
            <AccordionDetails>
                <Grid container direction='row'>
                    <Grid item xs={4}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DatePicker label='From' value={fromDate} onChange={(newValue) => setFromDate(newValue)}/>
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={4}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DatePicker label='To' value={toDate} minDate={fromDate} onChange={(newValue) => setToDate(newValue)}/>
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={2}>
                        <FormControl >
                            <InputLabel id="demo-simple-select-label">Sort by</InputLabel>
                            <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={sortBy}
                            label="sort by"
                            onChange={handleSortChange}
                            >
                            <MenuItem value={'popularity'}>popularity</MenuItem>
                            <MenuItem value={'relevancy'}>relevancy</MenuItem>
                            <MenuItem value={'publishedAt'}>publish date</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={2}>
                    <Button variant="contained" onClick={onSubmit}>Search</Button>
                    </Grid>
                </Grid>
            </AccordionDetails>
        </Accordion>
        {loading && <LinearProgress style={{marginLeft:2, marginRight: 2}}/>}
      </Box>
  );
}
