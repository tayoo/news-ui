import * as React from "react";
import {Grid} from '@mui/material';
import Articles from './Articles'
import Filter from './Filter'
import {getArticles} from '../services/newsApi'

export default function Home () {
    const [articles, setArticles] = React.useState([]);
    const [loading, setLoading] = React.useState(false);

    const fetchArticles = async (data) => {
        setLoading(true)
        const fetchedArticles = await getArticles(data)
        setArticles(fetchedArticles)
        setLoading(false)
    }

    React.useEffect(()=>{ 
        fetchArticles({
            query: 'Dune Tech'
        })
    },[])

  return (
    <Grid container spacing={4} direction='column'  style={{paddingTop: 30, padding: 40, paddingLeft: 80, paddingRight: 80, backgroundColor: '#F0F0F0'}} justifyContent='center'>
        <Grid item xs={4}>
            <Filter reloadParent={fetchArticles} loading={loading} />
        </Grid>
        <Grid item  xs={8}>
            <Articles data={articles}/>
        </Grid>
    </Grid>
  );
};