    const data = [
      {
        source: {
          id: "independent",
          name: "Independent"
        },
        author: "Kate Ng and Ellie Muir",
        title:
          "The 10 most unusual celebrity children’s names, from Apple to Exa Dark Sideræl",
        description: "Famous parents love giving their kids unique names",
        url:
          "https://www.independent.co.uk/lifestyle/celebritychildrennamesgwynethpaltrowgrimesb2308790.html",
        urlToImage:
          "https://www.marketscreener.com/images/twitter_MS_fdblanc.png",
        publishedAt: "2023-03-28T09:12:32Z",
        content:
          "Stay ahead of the trend in fashion and beyond with our free weekly Lifestyle Edit newsletter\r\nStay ahead of the trend in fashion and beyond with our free weekly Lifestyle Edit newsletter \r\nWhenever c… [+6872 chars]"
      },
      {
        source: {
          id: null,
          name: "MarketWatch"
        },
        author: "Sean Tucke, r",
        title:
          "Kelley Blue Book: Why EV owners are getting grumpy about home charging",
        description:
          "A JD Power survey found EV owners overall growing less satisfied with the experience of plugging in at home. Some brands' chargers got higher marks than others",
        url:
          "https://www.marketwatch.com/story/whyevownersaregettinggrumpyabouthomecharging94a9f8ca",
        urlToImage: "https://www.marketscreener.com/images/twitter_MS_fdblanc.png",
        publishedAt: "2023-03-28T09:09:49Z",
        content:
          "America is steadily switching to electricity for its driving fuel. But the early days of EV ownership are filled with bugs and small frustrations. Even the aspects of the experience that promise to b… [+3671 chars]"
      },
      {
        source: {
          id: null,
          name: "Marketscreener.com"
        },
        author: "MarketScreener",
        title: "Home battery sales in Germany may surge 59% this year study",
        description:
          "(marketscreener.com) German sales of batteries to store solar energy at home may jump 59% this year as more residents seek a cheaper, renewable and more reliable power supply, industry association BVES said on Tuesday.https://www.marketscreener.com/quote/stoc…",
        url:
          "https://www.marketscreener.com/quote/stock/LGCHEMLTD6494928/news/HomebatterysalesinGermanymaysurge59thisyearstudy43354084/?utm_medium=RSS&utm_content=20230328",
        urlToImage: "https://www.marketscreener.com/images/twitter_MS_fdblanc.png",
        publishedAt: "2023-03-28T09:09:49Z",
        content:
          '"We expect to be cracking the 1millionunit level in 2023," said BVES, presenting an industrycommissioned study undertaken by advisory group 3Energie Consulting.\r\nIt forecast sales of 385,000 units… [+1766 chars]'
      },
    {
      source: {
        id: "independent",
        name: "Independent"
      },
      author: "Kate Ng and Ellie Muir",
      title:
        "The 10 most unusual celebrity children’s names, from Apple to Exa Dark Sideræl",
      description: "Famous parents love giving their kids unique names",
      url:
        "https://www.independent.co.uk/lifestyle/celebritychildrennamesgwynethpaltrowgrimesb2308790.html",
      urlToImage:
        "https://www.marketscreener.com/images/twitter_MS_fdblanc.png",
      publishedAt: "2023-03-28T09:12:32Z",
      content:
        "Stay ahead of the trend in fashion and beyond with our free weekly Lifestyle Edit newsletter\r\nStay ahead of the trend in fashion and beyond with our free weekly Lifestyle Edit newsletter \r\nWhenever c… [+6872 chars]"
    },
    {
      source: {
        id: null,
        name: "MarketWatch"
      },
      author: "Sean Tucke, r",
      title:
        "Kelley Blue Book: Why EV owners are getting grumpy about home charging",
      description:
        "A JD Power survey found EV owners overall growing less satisfied with the experience of plugging in at home. Some brands' chargers got higher marks than others",
      url:
        "https://www.marketwatch.com/story/whyevownersaregettinggrumpyabouthomecharging94a9f8ca",
      urlToImage: "https://www.marketscreener.com/images/twitter_MS_fdblanc.png",
      publishedAt: "2023-03-28T09:09:49Z",
      content:
        "America is steadily switching to electricity for its driving fuel. But the early days of EV ownership are filled with bugs and small frustrations. Even the aspects of the experience that promise to b… [+3671 chars]"
    },
    {
      source: {
        id: null,
        name: "Marketscreener.com"
      },
      author: "MarketScreener",
      title: "Home battery sales in Germany may surge 59% this year study",
      description:
        "(marketscreener.com) German sales of batteries to store solar energy at home may jump 59% this year as more residents seek a cheaper, renewable and more reliable power supply, industry association BVES said on Tuesday.https://www.marketscreener.com/quote/stoc…",
      url:
        "https://www.marketscreener.com/quote/stock/LGCHEMLTD6494928/news/HomebatterysalesinGermanymaysurge59thisyearstudy43354084/?utm_medium=RSS&utm_content=20230328",
      urlToImage: "https://www.marketscreener.com/images/twitter_MS_fdblanc.png",
      publishedAt: "2023-03-28T09:09:49Z",
      content:
        '"We expect to be cracking the 1millionunit level in 2023," said BVES, presenting an industrycommissioned study undertaken by advisory group 3Energie Consulting.\r\nIt forecast sales of 385,000 units… [+1766 chars]'
    }
  ];
  
  module.exports = { data };
  