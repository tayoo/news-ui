import axios from 'axios'

export const getArticles = async ({query, from, to, sortBy}) => {
	let url = `http://localhost:5002/v1/articles?query=${query}`
	if (from) url += `&from=${from}`
	if (to) url += `&to=${to}`
	if (sortBy) url += `&sortBy=${sortBy}`
	const config = {
		method: "get",
		maxBodyLength: Infinity,
		url: url,
        headers: {caller: 'news-ui'}
	}

	return await axios(config)
		.then((response) => {
			return response.data.articles
		})
		.catch((error) => {
			console.log(error)
			return []
		})
}
